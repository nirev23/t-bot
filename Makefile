APP := $(shell basename $(shell git remote get-url origin) .git)
REGISTRY := ghcr.io/nirev23
#VERSION=$(git describe --abbrev=0 --tags)-$(shell git rev-parse --short HEAD)
VERSION=${CI_COMMIT_TAG}-${CI_COMMIT_SHORT_SHA}
TARGETOS=linux#linux darwin windows
TARGETARCH=amd64#amd64 arm64

format:
	gofmt -s -w ./

lint:
	golint

test:
	go test -v

get:
	go get

build: format get
	CGO_ENABLED=0 GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -v -o t-bot -ldflags "-X="github.com/nirev23/t-bot/cmd.appVersion=${VERSION}

image:
	docker build . -t ${REGISTRY}/${APP}:${VERSION}-${TARGETOS}-${TARGETARCH}  --build-arg TARGETARCH=${TARGETARCH} --build-arg TARGETOS=${TARGETOS}

push:
	docker push ${REGISTRY}/${APP}:${VERSION}-${TARGETOS}-${TARGETARCH}

clean:
	rm -rf kbot
	docker rmi ${REGISTRY}/${APP}:${VERSION}-${TARGETARCH}
	
